<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
                
            [
            'name' => 'yosf',
            'email' => 'yosef@gmail.com',
            'password' => 12345,
            'created_at' => date('Y-m-d G:i:s'),
            ],
            [
                'name' => 'ayelet',
                'email' => 'ayeler@gmail.com',
                'password' => 123456789,
                'created_at' => date('Y-m-d G:i:s'),
            ],       
        
        ]);
    }
}
