<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                'name'=> 'Asi',
                'email'=>'Asi@gmail.com',
                'password' => Hash::make('12345678'),
                'created_at' => date('y-m-d G:i:s'),
                ],
                [
                'name'=> 'Ori',
                'email'=>'Ori@jack.com',
                'password' =>  Hash::make('1020304050'),
                'created_at' => date('y-m-d G:i:s'),
                ],
            ]);
    }
  
}
