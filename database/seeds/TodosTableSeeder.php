<?php

use Illuminate\Database\Seeder;

class TodosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('todos')->insert([
                
            [
            'title' => 'Buy milk',
            'user_id' => 1,
            'created_at' => date('Y-m-d G:i:s'),
            ],
            [
                'title' => 'Buy eggs',
                'user_id' => 2,
                'created_at' => date('Y-m-d G:i:s'),
            ],
            [
                'title' => 'Buy Bananas',
                'user_id' => 1,
                'created_at' => date('Y-m-d G:i:s'),
            ],
        
        
        ]);
    }
}
