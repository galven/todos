<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello', function () {
    return "Hello world!";
});

Route::get('/student/{id?}', function ($id=" no student provided ") {
    return "Hello student " .$id;
})->name('students');

/* practies routes*/

Route::get('/customer/{id?}', function ($id = null) {
    if($id == null){
    return ("no customer was provided");
    }
    else {
    return "Hello customer " .$id;
    }
})->name('customer');

Route::resource('todos', 'TodoController')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
