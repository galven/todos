<!DOCTYPE html >
<html>
@yield('content')
@extends('layouts.app')
@section('content')

<head> This is your todo list </head>

<ul>
   @foreach($todos as $todo)
<li>
@if ($todo->status)
           <input type = 'checkbox' id ="{{$todo->id}}" checked>
@else
           <input type = 'checkbox' id ="{{$todo->id}}">
@endif

 @can('manager') <a href = "{{route('todos.edit',$todo->id)}}"> @endcan {{$todo->title}}</a>
      
</li>
@endforeach
</ul>
@can('manager')<a href = "{{route('todos.create')}}"> create a new todo </a>@endcan
<script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
               $.ajax({
                   url:"{{url('todos')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type: 'put',
                   contentType: 'application/json',
                   data:JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   processData:false,
                   success: function(data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log(errorThrown );
                   }
               });               
           });
       });
   </script>
@endsection

</html>
