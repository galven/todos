@yield('content')
@extends('layouts.app')
@section('content')
<h1>create new Todo</h1>

<form method = 'post' action = "{{action('TodoController@store')}}">
{{csrf_field()}} 
<div class = "form-group">
<label for = "title">what would you like todo</label>
<input type = "text" class = "form-control" name = "title">
</div>

<div class = "form-group">
 <input type = "submit" class = "form-control" name = "submit" value = "Save" >
</div>
</form>
@endsection